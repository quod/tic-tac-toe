package edu.jsu.mcis;

import org.junit.*;
import static org.junit.Assert.*;

public class TicTacToeTest {
    public String[][] grid;
    String player = "X";
    String winner = "";
    int counter = 0;

    @Test
    public void testInitialBoardIsEmpty() {
        TicTacToe game = new TicTacToe();
        grid = new String[3][3];
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                grid[i][j] = " ";
                assertEquals(" ", game.getMark(i,j));
            }
        }
    }
    
    @Test
    public void testMarkXInUpperRightCorner() {
        TicTacToe game = new TicTacToe();
        game.makeMark("X", 0, 2);
        assertEquals("X", game.getMark(0,2));
    }
    
    @Test
    public void testMarkOInBottomLeftCorner() {
        TicTacToe game = new TicTacToe();
        game.makeMark("O", 2, 0);
        assertEquals("O", game.getMark(2,0));
    }
    
    @Test
    public void testUnableToMarkOverExistingMark() {
        TicTacToe game = new TicTacToe();
        game.makeMark("X", 1, 1);
        game.makeMark("O", 1, 1);
        assertEquals("X", game.getMark(1, 1));
    }
    
    @Test
    public void testGameIsNotOverAfterTheFirstMark() {
        TicTacToe game = new TicTacToe();
        game.makeMark("X", 1, 1);
        game.checkForWinner();
        game.getWinner();
        assertTrue(winner.equals(""));
    }
    
    @Test
    public void testGameIsWonByXHorizontallyAcrossTopRow() {
        TicTacToe game = new TicTacToe();
        game.makeMark("X", 0, 0);
        game.makeMark("O", 2, 0);
        game.makeMark("X", 0, 1);
        game.makeMark("O", 1, 1);
        game.makeMark("X", 0, 2);
        game.checkForWinner();
        winner = game.getWinner();
        assertEquals(winner, "X");
    }
    
    @Test
    public void testGameIsOverByTieIfAllLocationsAreFilled() {
        TicTacToe game = new TicTacToe();
        game.makeMark("X", 0, 0);
        game.makeMark("O", 0, 1);
        game.makeMark("X", 1, 1);
        game.makeMark("O", 0, 2);
        game.makeMark("X", 2, 0);
        game.checkForWinner();
        game.getWinner();
        game.makeMark("O", 2, 2);
        game.checkForWinner();
        game.getWinner();
        game.makeMark("X", 2, 1);
        game.checkForWinner();
        game.getWinner();
        game.makeMark("O", 1, 0);
        game.checkForWinner();
        game.getWinner();
        game.makeMark("X", 1, 2);
        game.checkForWinner();
        game.getWinner();
        game.checkForTie();
        winner = game.getWinner();
        assertEquals(winner, "TIE");
    }
}
