package edu.jsu.mcis;

import javax.swing.*;
import java.util.Objects;

public class TicTacToe {
    private String[][] grid;
    private String player;
    private String winner;
    private int counter;
    private JPanel panel1;


    public TicTacToe() {
        grid = new String[3][3];
        player = "X";
        counter = 0;
        newGame();
    }

    public void newGame() {
        grid = new String[3][3];
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                grid[i][j] = " ";
            }
        }
    }

    private void switchPlayer() {
        if(player.equals("X")) player = "O";
            else if(player.equals("O")) player = "X";
    }

    String getMark(int i, int j) {
        if(grid[i][j].equals(" ")) return " ";
        else if(grid[i][j].equals("X")) return "X";
        else return "O";
    }

    void makeMark(String mark, int i, int j) {
        if(grid[i][j].equals(" ")) {
            if (mark.equals("X")) {
                grid[i][j] = "X";
            }
            else if (mark.equals("O")) {
                grid[i][j] = "O";
            }
        }
        counter++;
        switchPlayer();
    }

    void checkForWinner() {
        if (counter > 4) {
            if (grid[0][0].equals(grid[0][1])
                    && grid[0][1].equals(grid[0][2]))
                winner = grid[0][0];
            else if (grid[0][0].equals(grid[1][0])
                    && grid[1][0].equals(grid[2][0]))
                winner = grid[0][0];
            else if (grid[0][0].equals(grid[1][1])
                    && grid[1][1].equals(grid[2][2]))
                winner = grid[0][0];
            else if (grid[0][2].equals(grid[1][2])
                    && grid[1][2].equals(grid[2][2]))
                winner = grid[0][2];
            else if (grid[0][2].equals(grid[1][1])
                    && grid[1][1].equals(grid[0][2]))
                winner = grid[0][2];
            else if (grid[2][0].equals(grid[2][1])
                    && grid[2][1].equals(grid[2][2]))
                winner = grid[2][0];
            else winner = " ";
        }
    }

    String getWinner() {
        return winner;
    }

    void checkForTie() {
        if(counter == 9 && Objects.equals(winner, " "))
            winner = "TIE";
    }

    public void main(String[] args) {
        newGame();
    }
}
