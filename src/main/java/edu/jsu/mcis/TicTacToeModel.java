package edu.jsu.mcis;

public class TicTacToeModel {
    private int[][] grid;

    private TicTacToeModel() {
        grid = new int[3][3];
    }

    private String getMark(int i, int j) {
        if (grid[i][j] == 0) return "";
        else if (grid[i][j] == 1) return "X";
        else return "O";
    }

    private void makeMark(String mark, int i, int j) {
        if (grid[i][j] == 0) {
            if (mark.equals("X")) grid[i][j] = 1;
            else if (mark.equals("O")) grid[i][j] = 2;
            else if (mark.equals("")) grid[i][j] = 0;
        }
    }
}
