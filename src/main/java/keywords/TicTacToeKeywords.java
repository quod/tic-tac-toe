package keywords;

import edu.jsu.mcis.*;

import java.util.Objects;

public class TicTacToeKeywords {
	public TicTacToe game = new TicTacToe();
	public String[][] grid;
	public String player;
	public int counter;
	public String winner;
	
	public void startNewGame() {
		game.newGame();
	}
	
	public void makeMark(String mark, int row, int col) {
		if (Objects.equals(grid[row][col], " ")) {
			if (player.equals("X")) grid[row][col] = "X";
			else if (player.equals("O")) grid[row][col] = "O";
			else grid[row][col] = " ";
		}
		counter++;
	}
	
	public String getMark(int row, int col) {
		if (Objects.equals(grid[row][col], " ")) return " ";
		else if (Objects.equals(grid[row][col], "X")) return "X";
		else if (Objects.equals(grid[row][col], "O")) return "O";
		else return " ";
	}

	public void switchPlayer() {
		if (player.equals("X")) player = "O";
		else if (player.equals("O")) player = "X";
	}
    
	public String getWinner() {
		return winner;
	}
}
